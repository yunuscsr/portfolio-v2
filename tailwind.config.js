/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      dark: "#181C1A",
      primary: "#9DB0A3",
      secondary: "#3A4B40",
      white: "#FFFFFF",
    },

    extend: {
      fontFamily: {
        montreal: ["Montreal Yunus", "sans-serif"],
      },
    },
  },
  plugins: [],
};
