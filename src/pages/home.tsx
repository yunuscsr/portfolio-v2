import React from "react";

import ProfilePhoto from "../assets/profile.jpg";
import CosarVTCPreview from "../assets/cosar-vtc_prev.png";
import MonboprodPreview from "../assets/monboprod_prev.png";
import DivergentBeautyPreview from "../assets/divergent-beauty_prev.png";
import { motion } from "framer-motion";
import { useMediaQuery } from 'react-responsive';



import { BiLogoLinkedin } from "react-icons/bi";
import { Link } from "react-router-dom";
import TypeWriter from "../components/utils/typeWriter";

const Home = () => {

  const typeWriterWords = [ 'à Strasbourg', 'de 22 ans', 'Créatif', "à l'écoute", "passionné" ];
  const isBetween768And862 = useMediaQuery({ minWidth: 768, maxWidth: 862 });
  const isBetween768And847 = useMediaQuery({ minWidth: 768, maxWidth: 847 });
  const isMax370 = useMediaQuery({ maxWidth: 847 });

  return (
    <>
      {/* <div className="noisy-bg"></div> */}
      <div className="bg-dark w-full font-montreal min-h-screen m-0 overflow-x-hidden ">
        <div className="h-full md:h-screen  flex flex-col justify-between md:justify-start ">
          <div className="flex flex-col md:flex-row h-screen md:h-full">
            <div className="w-full h-1/2 md:w-2/3 px-5 sm:px-10 py-10 md:py-20 flex flex-col justify-between 2xl:justify-start">
              <h1>
                <motion.div
                   initial={{ top: isBetween768And862 ? "2.5em" : isMax370 ? "2em" : "1em" }}
                  animate={{ top: 0, transition: {
                    duration: 1.5,
                    ease: [0.83, 0, 0.17, 1],
                    delay: -0.25,
                  } }}
                  transition={{ delay: 1 }}
                  className="text-5xl bg-dark z-0 overflow-hidden relative sm:text-7xl xl:text-8xl 2xl:text-9xl uppercase font-extraold text-primary"
                >
                    yunus cosar
                </motion.div>
                <div className="reveal relative after:z-0 after:h-[10em] min-[862px]:after:h-[5.5em] after:w-full"></div>
              </h1>
            
              <div className="flex-col flex justify-start xl:justify-between">
                <div className="text-4xl sm:text-4xl md:text-6xl 2xl:text-8xl text-primary">
                  <h2>
                    <motion.div
                    initial={{ top: isBetween768And847 ? "3.5em" : "2.6em" }}
                    animate={{ top: 0, transition: {
                      duration: 1.5,
                      ease: [0.83, 0, 0.17, 1],
                      delay: 0.5,
                    } }}
                    transition={{ delay: 1 }}
                    className="relative z-10 uppercase font-extraold">
                      web developer
                      & designer
                    <TypeWriter words={typeWriterWords} />
                    </motion.div>
                    <div className="reveal relative after:z-20 after:w-full after:h-[3.5em] min-[847px]:after:h-[3em]"></div>
                  </h2>
                </div>
                  <a className="w-full mt-5 md:mt-16 z-20" href="#projects">
                    <motion.div
                      initial={{ left: "-150%" }}
                      animate={{ left: 0, transition: {
                        duration: 3,
                        ease: [0.83, 0, 0.17, 1],
                        delay: 0.5,
                      } }}
                      transition={{ delay: 1 }}
                      className="relative"
                    >
                      <button
                        className="btnHoverFill w-full px-5 py-3 sm:py-5 sm:px-10 relative border text-base sm:text-lg h-fit border-secondary hover:border-primary text-primary rounded-full uppercase tracking-widest overflow-hidden hover:text-dark transition-colors duration-700"
                        type="button"
                      >
                        <span className="absolute inset-0 bg-primary"></span>
                        <span className="absolute inset-0 flex justify-center items-center">
                          mes réalisations &darr;
                        </span>
                        mes réalisations &darr;
                      </button>
                    </motion.div>
                  </a>
              </div> 
            </div>
            <img
              className="w-full z-30 md:w-1/3 object-cover h-1/2 md:h-full"
              src={ProfilePhoto}
              alt="Yunus Cosar"
            ></img>
          </div>
          <div className="hidden border-t border-t-secondary relative z-30 sm:flex flex-col gap-2 sm:flex-row h-24 py-3 lg:py-0 items-center text-secondary text-base sm:text-[0.8rem] uppercase justify-between px-2 sm:px-10">
            <div>
              <a
                href="https://www.linkedin.com/in/yunuscosar/"
                target="_blank"
                rel="noreferrer"
                className="flex items-end gap-2"
              >
                <span className="text-3xl hover:text-white transition-colors">
                  <BiLogoLinkedin />
                </span>
                <p>/yunuscosar</p>
              </a>
            </div>
            <div>
              <p>
                Actuellement à la <br /> recherche d'un CDI
              </p>
            </div>
            <div>
              <p>
                deux années d'expérience <br /> en agence de com & RH
              </p>
            </div>
          </div>
          <div className="flex sm:hidden z-30 relative flex-col sm:flex-row h-24 items-center text-secondary text-base sm:text-[0.8rem] uppercase justify-between px-2 sm:px-10">
            <div className="border-b border-secondary w-full text-center flex flex-col items-center py-2">
              <a
                href="https://www.linkedin.com/in/yunuscosar/"
                target="_blank"
                rel="noreferrer"
                className="flex items-end gap-2"
              >
                <span className="text-3xl hover:text-white transition-colors">
                  <BiLogoLinkedin />
                </span>
              </a>
            </div>
            <div className="border-b border-secondary w-full text-center flex flex-col items-center py-2">
              <p>
                Actuellement à la recherche d'un CDI
              </p>
            </div>
            <div className="border-b border-secondary w-full text-center flex flex-col items-center py-2">
              <p>
                deux années d'expérience en agence de com & RH
              </p>
            </div>
          </div>
        </div>
        <div id="projects" className="px-2 mt-36">
          <div className="w-full px-5 sm:px-10 md:px-0 md:w-3/4 mx-auto">
          <div className="">
              <Link to="https://www.cosarvtc.fr" target="_blank">
                <div className="group w-full flex justify-between border-t-2 border-secondary h-24 sm:h-36 md:h-48 p-2">
                  <div className="flex" style={ { flex: 4}}>
                    <div className="sm:group-hover:w-56 lg:group-hover:w-96 group-hover:h-full group-hover:transition-all mr-[1em] w-0 h-full overflow-hidden transition-all duration-1000">
                      <img src={CosarVTCPreview} alt="" className="w-full h-full object-cover"/>
                    </div>
                      <h2 className="text-5xl sm:text-5xl md:text-7xl uppercase text-primary">COSAR VTC</h2>
                  </div>
                  <div className="text-primary">
                    <p>&#8599;</p>
                  </div>
                </div>
              </Link>
            </div>
            <div className="">
              <Link to="https://www.monboprod.fr/" target="_blank">
                <div className="group w-full flex justify-between border-t-2 border-secondary h-24 sm:h-36 md:h-48 p-2">
                  <div className="flex" style={ { flex: 4}}>
                    <div className="sm:group-hover:w-56 lg:group-hover:w-96 group-hover:h-full group-hover:transition-all mr-[1em] w-0 h-full overflow-hidden transition-all duration-1000">
                      <img src={MonboprodPreview} alt="" className="w-full h-full object-cover"/>
                    </div>
                      <h2 className="text-5xl sm:text-5xl md:text-7xl uppercase text-primary">Monboprod</h2>
                  </div>
                  <div className="text-primary">
                    <p>&#8599;</p>
                  </div>
                </div>
              </Link>
            </div>
            <div className="">
              <Link to="https://www.divergentbeauty.com/" target="_blank">
                <div className="group w-full flex justify-between border-t-2 border-secondary h-24 sm:h-36 md:h-48 p-2">
                  <div className="flex" style={ { flex: 4}}>
                    <div className="sm:group-hover:w-56 lg:group-hover:w-96 group-hover:h-full group-hover:transition-all mr-[1em] w-0 h-full overflow-hidden transition-all duration-1000">
                      <img src={DivergentBeautyPreview} alt="" className="w-full h-full object-cover"/>
                    </div>
                      <h2 className="text-5xl sm:text-5xl md:text-7xl uppercase text-primary">Divergent Beauty</h2>
                  </div>
                  <div className="text-primary">
                    <p>&#8599;</p>
                  </div>
                </div>
              </Link>
            </div>
          </div>
      </div>
      <div className="px-5 sm:px-10 md:px-0 w-full md:w-3/4 mx-auto pt-20 pb-32 flex flex-col gap-10 text-primary uppercase">
        <div className=" text-sm tracking-wider leading-6 ">
          <p>Passionné par le développement et le design web, je suis prêt à vous emmener vers une nouvelle dimension digitale.</p>
          <p>Nous pouvons collaborer pour transformer votre présence en ligne en une expérience mémorable pour vos clients.</p>
          <p>Osez franchir le pas vers cette nouvelle ère du digital et propulsez votre entreprise vers de nouveaux horizons.</p>
          <p>Contactez-moi, et ensemble, façonnons le futur numérique de votre marque.</p>
        </div>
        <div className="flex flex-col gap-2 items-center">
          <p className="text-xs">yunuscosar.pro@gmail.com</p>
          <a className="w-full" href="mailto:yunuscosar.pro@gmail.com">
                  <button
                    className="btnHoverFill w-full px-5 py-3 sm:py-5 sm:px-10 relative border text-base sm:text-lg h-fit border-secondary hover:border-primary text-primary rounded-full uppercase tracking-widest overflow-hidden hover:text-dark transition-colors duration-700"
                    type="button"
                  >
                    <span className="absolute inset-0 bg-primary"></span>
                    <span className="absolute inset-0 flex justify-center items-center">
                      m'envoyer un mail &#8599;
                    </span>
                    m'envoyer un mail &#8599;
                  </button>
          </a>
        </div>
        
      </div>
      </div>
    </>
  );
};

export default Home;
